import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';
import {VueCsvImport} from "vue-csv-import/src";
import Home from "./components/Home";
import ProcessOrders from "./components/ProcessOrders";
import OrderBatch from "./components/OrderBatch";
import Order from "./components/Order";
import Batches from "./components/Batches";
import ManualShippingBatch from "./components/ManualShippingBatch";
import Offers from "./components/Offers";
import EditOffer from "./components/EditOffer";
import Orders from "./components/Orders";
import EditOrder from "./components/EditOrder";
import Files from "./components/Files";
import Options from "./components/Options";
import EditShippingMethod from "./components/EditShippingMethod";
import OrderFiles from "./components/OrderFiles";
import ShippingLabel from "./components/ShippingLabel";

Vue.config.productionTip = false
// Vue.prototype.$backend = "http://192.168.100.21:81";
// Vue.prototype.$backend = "https://backend.nl.jordio.local";
// Vue.prototype.$backend = "http://jordio.backend.test";
// Vue.prototype.$backend = "https://test.backend.nl.jordio.local";
Vue.prototype.$backend = process.env.JORDIO_BACKEND_SERVER;

Vue.use(VueRouter);
Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
Vue.component('vue-csv-import', VueCsvImport);
Vue.use(VueAxios, axios)

const routes = [
    {path: '/', name:'Home', component: Home},
    {path: '/process/orders', name:'process-orders', component: ProcessOrders},
    {path: '/orders', name: 'Orders', component: Orders},
    {path: '/orders/:id', name: 'Order', component: Order},
    {path: '/orders/:id/files', name: 'OrderFiles', component: OrderFiles},
    {path: '/orders/:id/edit', name: 'EditOrder', component: EditOrder},
    {path: '/batches', name:'Batches', props:true, component: Batches,},
    {path: '/order-batch/:id', name:'orderBatch', props:true, component: OrderBatch,},
    {path: '/order-batch/:id/manual-shipping', name:'ManualShippingBatch', props:true, component: ManualShippingBatch,},
    {path: '/offers', name: 'Offers', component: Offers},
    {path: '/offers/:id', name: 'EditOffer', component: EditOffer},

    {path: '/shipping-label', name: 'ShippingLabel', component: ShippingLabel},
    {path: '/files', name: 'Files', component: Files},
    {path: '/options', name: 'Options', component: Options},

    {path: '/shipping-method/:id/edit', name: 'EditShippingMethod', component: EditShippingMethod},
];

const router = new VueRouter({
    routes: routes,
    mode: 'history'
});

new Vue({
    vuetify,
    router,
    render: h => h(App)
}).$mount('#app')
