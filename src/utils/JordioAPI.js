import axios from "axios";
import moment from "moment";

const blobToFile = (blob, name, extension) => {
    const now = moment().format('YYYY-MM-DDThh_mm_ss');
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.setAttribute('download', `${name}_${now}.${extension}`);
    link.click();
}

const exportOrders = (orders) => {
    return axios.post(process.env.VUE_APP_JORDIO_BACKEND_SERVER + '/orders/spreadsheet', orders, {
        responseType: 'blob'
    })
        .then(response => {
            if (response.headers['content-type'] === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                const blob = new Blob([response.data]);
                blobToFile(blob, 'bol_orders_export', 'xlsx');
            }
        })
};

const getPackingSlips = (orders) => {
    return axios.post(process.env.VUE_APP_JORDIO_BACKEND_SERVER + '/orders/packing-slips', orders, {
        responseType: 'arraybuffer'
    })
        .then(response => {
            if (response.headers['content-type'] === 'application/pdf') {
                const blob = new Blob([response.data], {type: 'application/pdf'})
                blobToFile(blob, 'bol_packing_slips', 'pdf');
            }
        })
};

const getInvoices = (orders) => {
    return axios.post(process.env.VUE_APP_JORDIO_BACKEND_SERVER + '/orders/invoices', orders, {
        responseType: 'arraybuffer'
    })
        .then(response => {
            if (response.headers['content-type'] === 'application/pdf') {
                const blob = new Blob([response.data], {type: 'application/pdf'})
                blobToFile(blob, 'bol_invoices', 'pdf');
            }
        })
};

const getShippingLabels = (orders) => {
    return axios.post(process.env.VUE_APP_JORDIO_BACKEND_SERVER + '/shipments/labels/postnl', orders, {
        responseType: 'arraybuffer'
    })
        .then(response => {
            if(response.headers['content-type'] === 'application/pdf') {
                const blob = new Blob([response.data], {type: 'application/pdf'})
                blobToFile(blob, 'bol_shipping_labels', 'pdf');
            }
        })
};

const JordioAPI = {
    exportOrders, getPackingSlips, getShippingLabels, getInvoices
}

export default JordioAPI


