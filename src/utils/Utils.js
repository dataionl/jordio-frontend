function companyFieldHasVATNumber(company) {
    if (company) {
        let rawString = company.replace(/\./g, "");
        let match = rawString.match(/[0-9]{6,8}/gm);
        if (match !== null) {
            return true
        }
    }
    return false;
}

export const shipmentDetailsCheck = (orders) => {
    let warningReasons = [];
    orders.forEach(order => {
        let shipmentWarnings = [];
        //if the extraAddressInformation contains ANYTHING
        if (order.shipmentDetails.extraAddressInformation) {
            shipmentWarnings.push("extra-adres-informatie: " + order.shipmentDetails.extraAddressInformation);
        }

        //if company contains something that looks like a VAT-number
        let company = order.shipmentDetails.company
        if (company && companyFieldHasVATNumber(company)) {
            shipmentWarnings.push("bedrijfsnaam: " + order.shipmentDetails.company);
        }

        //if houseNumberExtension contains something that looks like a VAT-number
        //or if houseNumberExtension contains 7 or more characters
        let houseNumberExtension = order.shipmentDetails.houseNumberExtension;
        if (houseNumberExtension) {
            let rawString = houseNumberExtension.replace(/\./g, "");
            let match = rawString.match(/[0-9]{6,8}/gm);
            if (match !== null) {
                shipmentWarnings.push("huisnummer-toevoeging (btw-nummer?): " + order.shipmentDetails.houseNumberExtension);
            } else if (houseNumberExtension.length >= 7) {
                shipmentWarnings.push("huisnummer-toevoeging (>=7 karakters): " + order.shipmentDetails.houseNumberExtension);
            }
        }

        order.shipmentWarnings = shipmentWarnings;
        if (shipmentWarnings.length > 0) {
            shipmentWarnings.forEach(reason => {
                warningReasons.push(order.orderId + ": " + reason)
            })
        }
    });
    return {
        orders,
        warningReasons,
    }
};

export const setOrderStatus = (order) => {
    let statuses = [];

    order.orderItems.forEach((orderItem) => {
        statuses.push(orderItem.status);
    })
    order.status = statuses;
    return order;
}

export {
    companyFieldHasVATNumber,
}
