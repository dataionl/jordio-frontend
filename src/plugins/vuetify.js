import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: false,
        themes: {
            light: {
                primary: process.env.VUE_APP_PRIMARY_COLOR,
                secondary: process.env.VUE_APP_SECONDARY_COLOR,
                tertiary: process.env.VUE_APP_TERTIARY_COLOR,
                quaternary: process.env.VUE_APP_QUATERNARY_COLOR,
            },
            dark: {
                primary: '#7289DA',
                secondary: '#4E5D94',
            }
        },
        options: {
            customProperties: true
        }
    },
});
