# jordio.frontend

## Project setup
```bash
npm install
```

### Compiles and hot-reloads for development
```bash
npm run serve
```

### Compiles and minifies for production
```bash
npm run build
```

#### Build with a specific environment configuration file
```bash
npm run build -- --mode NL
```

### Lints and fixes files
```bash
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Contact
Contact me on [jordiholleman@gmail.com](jordiholleman@gmail.com)